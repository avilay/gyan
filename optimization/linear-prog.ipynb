{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "c81724a4-8732-429a-8afc-59f36541bc66",
   "metadata": {},
   "outputs": [],
   "source": [
    "import math\n",
    "import numpy as np\n",
    "import plotly.graph_objects as go\n",
    "from scipy.optimize import linprog"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "4f0226a1-5026-4241-ac9c-da3c898d2ed0",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.set_printoptions(edgeitems=30, linewidth=100000, precision=3)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e128d5d-6589-4126-bdf4-e82a7e35801c",
   "metadata": {},
   "source": [
    "# Linear Programming\n",
    "\n",
    "These problems have the following form:\n",
    "\n",
    "We have to minimze a linear combination of a vector $\\mathbf x$ \n",
    "\n",
    "$$\n",
    "min_{\\mathbf x} \\mathbf c^T \\mathbf x\n",
    "$$\n",
    "\n",
    "subject to:\n",
    "\n",
    "$$\n",
    "A_{ub} \\mathbf x <= \\mathbf b_{ub} \\\\\n",
    "A_{eq} \\mathbf x = \\mathbf b_{eq} \\\\\n",
    "l <= x <= u \\\\\n",
    "$$\n",
    "\n",
    "For example:\n",
    "\n",
    "$$\n",
    "max \\; 29x_1 + 45x_2\n",
    "$$\n",
    "\n",
    "Subject to:\n",
    "$$\n",
    "x_1 - x_2 - 3x_3 <= 5 \\\\\n",
    "2x_1 - 3x_2 - 7x_3 + 3x_4 >= 10 \\\\\n",
    "2x_1 + 8x_2 + x_3 = 60 \\\\\n",
    "4x_1 + 4x_2 + x_4 = 60 \\\\\n",
    "0 <= x_0 \\\\\n",
    "0 <= x_1 <= 5 \\\\\n",
    "x_2 <= 0.5 \\\\\n",
    "-3 <= x_3 \\\\\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cba49013-cf60-4ffb-86ad-5ae1e6c54fa7",
   "metadata": {},
   "source": [
    "It is easily seen that the example problem is not in the same shape/form as the needed by Scipy. First we need to transform this to a minimization problem instead of its current maximization form. And the objective function should be a function of all the variables.\n",
    "\n",
    "$$\n",
    "min \\; -29x_1 - 45x_2 + 0x_3 + 0x_4\n",
    "$$\n",
    "\n",
    "This means that the coefficient vector $\\mathbf c$ is -\n",
    "\n",
    "$$\n",
    "\\mathbf c = \\begin{bmatrix}\n",
    "-29 \\\\\n",
    "-45 \\\\\n",
    "0 \\\\\n",
    "0 \\\\\n",
    "\\end{bmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "82440827-d922-4a69-9088-146edbe627ed",
   "metadata": {},
   "source": [
    "Now both the inequality constraints must be in the $<=$ form and again have all the variables.\n",
    "\n",
    "$$\n",
    "x_1 - x_2 - 3x_3 + 0x_4 <= 5 \\\\\n",
    "-2x_1 + 3x_2 + 7x_3 - 3x_4 <= 10 \\\\\n",
    "$$\n",
    "\n",
    "Putting this in the $A_{ub} \\mathbf x <= \\mathbf b_{ub}$ we get -\n",
    "\n",
    "$$\n",
    "A_{ub} = \\begin{bmatrix}\n",
    "1 & -1 & -3 & 0 \\\\\n",
    "-2 & 3 & 7 & -3 \\\\\n",
    "\\end{bmatrix}\n",
    "$$\n",
    "\n",
    "and -\n",
    "$$\n",
    "\\mathbf b_{ub} = \\begin{bmatrix}\n",
    "5 \\\\\n",
    "-10 \\\\\n",
    "\\end{bmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "746bfb1c-3bd8-4157-983f-2eec0290ba7d",
   "metadata": {},
   "source": [
    "Now, the equality constraints with all the variables -\n",
    "\n",
    "$$\n",
    "2x_1 + 8x_2 + 1x_3 + 0x_4 = 60 \\\\\n",
    "4x_1 + 4x_2 + 0x_3 + 1x_4 = 60 \\\\\n",
    "$$\n",
    "\n",
    "$$\n",
    "A_{eq} = \\begin{bmatrix}\n",
    "2 & 8 & 1 & 0 \\\\\n",
    "4 & 4 & 0 & 1 \\\\\n",
    "\\end{bmatrix}\n",
    "$$\n",
    "\n",
    "$$\n",
    "b_{eq} = \\begin{bmatrix}\n",
    "60 \\\\\n",
    "60 \\\\\n",
    "\\end{bmatrix}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b8924d77-2d40-4219-b89b-d4b2a942f858",
   "metadata": {},
   "source": [
    "The bound constraints are simply expressed as a tuple of lower and upper bounds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "3f5f0e83-1af8-4a6a-9c3d-d62200c1516c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# None can be used instead of np.inf for upper bounds.\n",
    "x0_bounds = (0, np.inf)\n",
    "x1_bounds = (0, 5)\n",
    "x2_bounds = (-np.inf, 0.5)\n",
    "x3_bounds = (-3, np.inf)\n",
    "bounds = [x0_bounds, x1_bounds, x2_bounds, x3_bounds]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "f493ffb4-d951-4e75-9226-f41bf1cffc02",
   "metadata": {},
   "outputs": [],
   "source": [
    "c = np.array([-29, -45, 0, 0], dtype=np.float64)\n",
    "\n",
    "A_ub = np.array([\n",
    "    [-29, -45, 0, 0],\n",
    "    [-2, 3, 7, -3]\n",
    "], dtype=np.float64)\n",
    "b_ub = np.array([5, -10], dtype=np.float64)\n",
    "\n",
    "A_eq = np.array([\n",
    "    [2, 8, 1, 0],\n",
    "    [4, 4, 0, 1]\n",
    "], dtype=np.float64)\n",
    "b_eq = np.array([60, 60], dtype=np.float64)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "cf71e896-e18d-4327-af07-718faf36be8d",
   "metadata": {},
   "outputs": [],
   "source": [
    "minima = linprog(\n",
    "    c, \n",
    "    A_ub=A_ub, b_ub=b_ub,\n",
    "    A_eq=A_eq, b_eq=b_eq,\n",
    "    bounds=bounds\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "a3608046-380b-4129-957b-6e93393d8902",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "     con: array([39.919, 42.686])\n",
       "     fun: -180.0529218554429\n",
       " message: 'The algorithm terminated successfully and determined that the problem is infeasible.'\n",
       "     nit: 8\n",
       "   slack: array([185.053,  14.618])\n",
       "  status: 2\n",
       " success: False\n",
       "       x: array([ 2.266,  2.541, -4.778, -1.913])"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "minima"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "120f81b3-5b1f-4f53-b4ca-cafcc7074bd1",
   "metadata": {},
   "source": [
    "Note the `status=False` and the message that says this is an infeasible problem.\n",
    "\n",
    "Lets change the bounds for $0 <= x_1 <= 6$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "832ed8a8-ba9c-4e8f-b150-60c8ff8fe005",
   "metadata": {},
   "outputs": [],
   "source": [
    "x1_bounds = (0, 6)\n",
    "bounds = [x0_bounds, x1_bounds, x2_bounds, x3_bounds]\n",
    "minima = linprog(\n",
    "    c, \n",
    "    A_ub=A_ub, b_ub=b_ub,\n",
    "    A_eq=A_eq, b_eq=b_eq,\n",
    "    bounds=bounds\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "8709870b-0d18-4dbd-bf16-8427fc331f0f",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "     con: array([6.064e-09, 6.484e-09])\n",
       "     fun: -552.7499999367897\n",
       " message: 'Optimization terminated successfully.'\n",
       "     nit: 7\n",
       "   slack: array([557.75,  35.  ])\n",
       "  status: 0\n",
       " success: True\n",
       "       x: array([ 9.75,  6.  , -7.5 , -3.  ])"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "minima"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "08d0b7f6-d570-42d3-b0af-046fec92b8bc",
   "metadata": {},
   "source": [
    "To tradeoff speed for accuracy we can use `revised_simplex`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "0859d727-72c8-4193-8c9b-03c233b9e230",
   "metadata": {},
   "outputs": [],
   "source": [
    "minima = linprog(\n",
    "    c, \n",
    "    A_ub=A_ub, b_ub=b_ub,\n",
    "    A_eq=A_eq, b_eq=b_eq,\n",
    "    bounds=bounds,\n",
    "    method=\"revised simplex\"\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "fea17380-303e-45f1-9e39-a743befe1784",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "     con: array([0., 0.])\n",
       "     fun: -552.75\n",
       " message: 'Optimization terminated successfully.'\n",
       "     nit: 5\n",
       "   slack: array([557.75,  35.  ])\n",
       "  status: 0\n",
       " success: True\n",
       "       x: array([ 9.75,  6.  , -7.5 , -3.  ])"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "minima"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "id": "e695b8d3-0612-4a93-8406-d85d770ce054",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "-552.75"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "c @ minima.x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "260f39ce-a22d-4165-a3b2-66824945ba2b",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
