% Gyan
% Avilay Parekh

# Gyan

  1. [Linear Algebra](./linear-algebra/index.html)
  2. [Reinforcement Learning](./reinforcement-learning/index.html)
  3. [Quantum Computing](./quantum-computing/index.html)
  4. [Deep Learning](./deep-learning/index.html)
  5. [Docker/Kubernetes](./dockerk8/index.html)
