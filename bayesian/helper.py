import numpy as np
import pandas as pd
import plotly.express as px
from scipy.special import gamma


def build_beta(a, b):
    B = (gamma(a) * gamma(b)) / gamma(a + b)

    def beta(x):
        return np.power(x, a - 1) * np.power(1 - x, b - 1) / B

    return beta


def plot_beta(*args):
    x = np.linspace(0, 1, 100)
    data = {}
    for (a, b) in args:
        beta_fn = build_beta(a, b)
        y = beta_fn(x)
        data[f"(α={a}, β={b})"] = y
    df = pd.DataFrame(data, index=x)
    fig = px.line(
        df,
        labels={"index": "x", "value": "y"},
        width=600,
        height=500,
        line_shape="spline",
    )
    return fig
