import numpy as np
from torch.utils.data import Dataset
import more_itertools as it


def trend(times, slope):
    return slope * times


def seasonal_pattern(season_times):
    """
    The first 10% of the season has values cos(7πt) and remaining 90% have values 1/exp(5t)

    Args:
        season_times (ndarray): An array with values between 0.0 and 1.0 but repeating in a pattern.
        e.g., 0.0, 0.25, 0.5, 0.75, 1.0, 0.0, 0.25, 0.5, 0.75, ...

    Returns:
        ndarray: of the same length as season_times, except the first 10% of the season is one function and the remaining 90% is another function.
    """
    return np.where(
        season_times < 0.4, np.cos(season_times * 2 * np.pi), 1 / np.exp(3 * season_times)
    )


def seasonality(times, period, amplitude=1, phase=0):
    # Convert times into a repeating array with values between 0 and 1.
    season_times = ((times + phase) % period) / period
    return amplitude * seasonal_pattern(season_times)


def noise(times, noise_level, seed=None):
    rnd = np.random.RandomState(seed)
    return rnd.standard_normal(len(times)) * noise_level


def gen_series():
    times = np.arange(4 * 365 + 1, dtype=np.float32)
    baseline = 10
    amplitude = 40
    slope = 0.05
    noise_level = 5
    series = (
        baseline
        + trend(times, slope)
        + seasonality(times, period=365, amplitude=amplitude)
        + noise(times, noise_level, seed=42)
    )

    split_time = 1000
    train_times = times[:split_time]
    train_series = series[:split_time]
    print(train_times.shape, train_series.shape)

    val_times = times[split_time:]
    val_series = series[split_time:]
    print(val_times.shape, val_series.shape)

    return (train_times, train_series), (val_times, val_series)


class WindowedDataset(Dataset):
    def __init__(self, series):
        self._x = None
        self._y = None
        self._series = series

    def set_window_size(self, val):
        x, y = [], []
        for chunk in it.windowed(self._series, val):
            chunk = list(chunk)
            x.append(chunk[:-1])
            y.append(chunk[-1])
        self._x = np.array(x, dtype=np.float32)
        self._y = np.array(y, dtype=np.float32)

    def __len__(self):
        if self._x is None:
            raise RuntimeError("Must set window size first!")
        return self._x.shape[0]

    def __getitem__(self, idx):
        if self._x is None:
            raise RuntimeError("Must set window size first!")
        return self._x[idx], self._y[idx]
