import os
import os.path as path


class DirMaker:
    _instance = None

    def __init__(self):
        self._made_dirs: Set[str] = set()

    @classmethod
    def make(cls, dirpath: str) -> None:
        if cls._instance is None:
            cls._instance = DirMaker()
        cls._instance._make(dirpath)

    def _make(self, dirpath: str) -> None:
        if dirpath not in self._made_dirs:
            os.makedirs(dirpath, exist_ok=True)
            self._made_dirs.add(dirpath)
    