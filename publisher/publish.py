import os
import os.path as path
import shutil
from typing import Dict, Callable
import subprocess

from .dir_maker import DirMaker

processors: Dict[str, Callable[[str], None]] = {}


def walk(cwd: str = None) -> None:
    for node in os.listdir(cwd):
        if node == ".git":
            continue
        fullpath = path.join(cwd, node) if cwd else node
        if path.isfile(fullpath):
            process(fullpath)
        else:
            walk(fullpath)


def process(filepath: str) -> None:
    ext = path.splitext(filepath)[1]
    process = processors.get(ext, lambda x: None)
    process(filepath)


def copy_file(filepath: str) -> None:
    dirname = path.join("public", path.dirname(filepath))
    dstfile = path.join(dirname, path.basename(filepath))
    DirMaker.make(dirname)
    dstfile = path.join(dirname, path.basename(filepath))
    shutil.copyfile(filepath, dstfile)


def process_md(mdfile: str) -> None:
    dirname = path.join("public", path.dirname(mdfile))
    DirMaker.make(dirname)
    htmlfile = path.join(dirname, path.basename(mdfile).replace(".md", ".html"))
    subprocess.run(
        ["pandoc", "-s", "-c", "/gyan/markdown.css", "--mathjax", mdfile, "-o", htmlfile]
    )


def process_ipynb(notebook: str) -> None:
    dirname = path.join("public", path.dirname(notebook))
    DirMaker.make(dirname)
    subprocess.run(
        [
            "jupyter",
            "nbconvert",
            "--to",
            "html",
            "--template",
            "full",
            "--output-dir",
            dirname,
            notebook,
        ]
    )


def main():
    # processors[".png"] = copy_file
    processors[".ipynb"] = process_ipynb
    processors[".md"] = process_md
    # processors[".css"] = copy_file

    exts_to_copy = [
        ".png",
        ".css",
        ".json",
        ".jsonp",
        ".jpeg",
        ".html",
        ".pdf",
        ".pdfp",
        ".js",
        ".bcmap",
    ]
    for ext_to_copy in exts_to_copy:
        processors[ext_to_copy] = copy_file

    walk()


if __name__ == "__main__":
    main()
